package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.flowerPackage.*;
import com.epam.rd.java.basic.task8.Flowers;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

    private StringBuilder currentValue = new StringBuilder();
    private List<Flower> list = new ArrayList<>();
    private static final int INIT = 0;
    private Flowers fls;
    private Flower fl;
    private VisualParameters parameters;
    private GrowingTips tips;
    private AveLenFlower aveLenFl;
    private Tempreture tempreture;
    private Watering watering;

    private String xmlFileName;

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    @Override
    public void startDocument() throws SAXException {
        fls = new Flowers();
    }

    @Override
    public void startElement(String uri, String localName,
                             String qName, Attributes attributes) throws SAXException {
        //reset value
        currentValue.setLength(0);

        if (qName.equalsIgnoreCase("flower")) {
            fl = new Flower();
            parameters = new VisualParameters();
            tips = new GrowingTips();
            aveLenFl = new AveLenFlower();
            tempreture = new Tempreture();
            watering = new Watering();
        }

        if (qName.equalsIgnoreCase("aveLenFlower")) {
            String measure = attributes.getValue("measure");
            aveLenFl.setMeasure(measure);
        }
        if (qName.equalsIgnoreCase("tempreture")) {
            String measure = attributes.getValue("measure");
            tempreture.setMeasure(measure);
        }
        if (qName.equalsIgnoreCase("lighting")) {
            String value = attributes.getValue("lightRequiring");
            tips.setLighting(new Lighting(value));
        }
        if (qName.equalsIgnoreCase("watering")) {
            String measure = attributes.getValue("measure");
            watering.setMeasure(measure);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("name")) {
            fl.setName(currentValue.toString());
        }
        if (qName.equalsIgnoreCase("soil")) {
            fl.setSoil(currentValue.toString());
        }
        if (qName.equalsIgnoreCase("origin")) {
            fl.setOrigin(currentValue.toString());
        }
        if (qName.equalsIgnoreCase("stemColour")) {
            parameters.setStemColour(currentValue.toString());
        }
        if (qName.equalsIgnoreCase("leafColour")) {
            parameters.setLeafColour(currentValue.toString());
        }
        if (qName.equalsIgnoreCase("aveLenFlower")) {
            int value = Integer.parseInt(currentValue.toString());
            aveLenFl.setValue(value);
            parameters.setAveLenFlower(aveLenFl);
        }
        if (qName.equalsIgnoreCase("tempreture")) {
            int value = Integer.parseInt(currentValue.toString());
            tempreture.setValue(value);
            tips.setTempreture(tempreture);
        }
        if (qName.equalsIgnoreCase("watering")) {
            int value = Integer.parseInt(currentValue.toString());
            watering.setValue(value);
            tips.setWatering(watering);
        }
        if (qName.equalsIgnoreCase("multiplying")) {
            fl.setMultiplying(currentValue.toString());
        }
        if (qName.equalsIgnoreCase("flower")) {
            fl.setGrowingTips(tips);
            fl.setParameters(parameters);
            list.add(fl);
        }
        if (qName.equalsIgnoreCase("flowers")) {
            fls.setFlowersList(list);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        currentValue.append(ch, start, length);
    }

    public Flowers getFlowersFromXML() throws ParserConfigurationException, SAXException, IOException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();
        saxParser.parse(xmlFileName, this);
        return fls;
    }
    //startDocument --> startElement --> characters --> endElement -->
    //				--> startElement --> characters --> endElement --> endDocument
}