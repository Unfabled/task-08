package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.flowerPackage.*;
import com.epam.rd.java.basic.task8.Flowers;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private String xmlFileName;
    private Flowers fls;
    private Flower fl;
    private List<Flower> list = new ArrayList<>();
    private VisualParameters parameters;
    private GrowingTips tips;

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public Flowers getFlowersFromXML() {
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        try {
            XMLEventReader reader = inputFactory.createXMLEventReader(new FileInputStream(xmlFileName));
            while (reader.hasNext()) {
                XMLEvent event = reader.nextEvent();
                if (event.isStartElement()) {
                    StartElement startElement = event.asStartElement();
                    switch (startElement.getName().getLocalPart()) {
                        case "flower":
                            fl = new Flower();
                            parameters = new VisualParameters();
                            tips = new GrowingTips();
                            break;
                        case "name":
                            event = reader.nextEvent();
                            fl.setName(event.asCharacters().getData());
                            break;
                        case "soil":
                            event = reader.nextEvent();
                            fl.setSoil(event.asCharacters().getData());
                            break;
                        case "origin":
                            event = reader.nextEvent();
                            fl.setOrigin(event.asCharacters().getData());
                            break;
                        case "stemColour":
                            event = reader.nextEvent();
                            parameters.setStemColour(event.asCharacters().getData());
                            break;
                        case "leafColour":
                            event = reader.nextEvent();
                            parameters.setLeafColour(event.asCharacters().getData());
                            break;
                        case "aveLenFlower":
                            Attribute measure = startElement.getAttributeByName(new QName("measure"));
                            event = reader.nextEvent();
                            AveLenFlower aveLenFl = null;
                            if (measure != null) {
                                 aveLenFl = new AveLenFlower(
                                        measure.getValue(), Integer.parseInt(event.asCharacters().getData()));
                            }
                            parameters.setAveLenFlower(aveLenFl);
                            break;
                        case "tempreture":
                            measure = startElement.getAttributeByName(new QName("measure"));
                            event = reader.nextEvent();
                            Tempreture tempreture = null;
                            if (measure != null) {
                                tempreture = new Tempreture(
                                        measure.getValue(), Integer.parseInt(event.asCharacters().getData()));
                            }
                            tips.setTempreture(tempreture);
                            break;
                        case "lighting":
                            Attribute lightRequiring = startElement.getAttributeByName(new QName("lightRequiring"));
                            tips.setLighting(new Lighting(lightRequiring.getValue()));
                        case "watering":
                            measure = startElement.getAttributeByName(new QName("measure"));
                            event = reader.nextEvent();
                            Watering watering = null;
                            if (measure != null) {
                                watering = new Watering(
                                        measure.getValue(), Integer.parseInt(event.asCharacters().getData()));
                            }
                            tips.setWatering(watering);
                            break;
                        case "multiplying":
                            event = reader.nextEvent();
                            fl.setMultiplying(event.asCharacters().getData());
                            break;
                    }
                }
                if (event.isEndElement()) {
                    EndElement endElement = event.asEndElement();
                    switch (endElement.getName().getLocalPart()) {
                        case "visualParameters" :
                            fl.setParameters(parameters);
                            break;
                        case "growingTips":
                            fl.setGrowingTips(tips);
                            break;
                        case "flower":
                            list.add(fl);
                            break;
                        case "flowers":
                            fls = new Flowers(list);
                            break;
                    }
                }
            }
        } catch (IOException | XMLStreamException e) {
            e.printStackTrace();
        }
        return fls;
    }
}