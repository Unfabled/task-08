package com.epam.rd.java.basic.task8.flowerPackage;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "lighting")

public class Lighting {
    @XmlAttribute(name = "lightRequiring")
    private String lightRequiring;

    public Lighting() {
    }

    public Lighting(String lightRequiring) {
        this.lightRequiring = lightRequiring;
    }

    public String getLightRequiring() {
        return lightRequiring;
    }

    public void setLightRequiring(String lightRequiring) {
        this.lightRequiring = lightRequiring;
    }

    @Override
    public String toString() {
        return "Lighting{" +
                "lightRequiring='" + lightRequiring + '\'' +
                '}';
    }
}