package com.epam.rd.java.basic.task8.flowerPackage;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "watering")

public class Watering {
    @XmlAttribute(name = "measure")
    private String measure;
    @XmlValue
    private int value;

    public Watering() {
    }

    public Watering(String measure, int value) {
        this.measure = measure;
        this.value = value;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "aveLenFlower{" +
                "measure='" + measure + '\'' +
                ", value=" + value +
                '}';
    }

}
