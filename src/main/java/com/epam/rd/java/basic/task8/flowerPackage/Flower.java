package com.epam.rd.java.basic.task8.flowerPackage;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "flower")
public class Flower {

    @XmlElement
    private String name;
    @XmlElement
    private String soil;
    @XmlElement
    private String origin;
    @XmlElementRef(name = "visualParameters")
    private VisualParameters parameters;
    @XmlElementRef(name = "growingTips")
    private GrowingTips growingTips;
    @XmlElement
    private String multiplying;

    public Flower() {
    }

    public Flower(String name, String soil, String origin, VisualParameters parameters,
                  GrowingTips growingTips, String multiplying) {
        this.name = name;
        this.soil = soil;
        this.origin = origin;
        this.parameters = parameters;
        this.growingTips = growingTips;
        this.multiplying = multiplying;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setParameters(VisualParameters parameters) {
        this.parameters = parameters;
    }

    public void setGrowingTips(GrowingTips growingTips) {
        this.growingTips = growingTips;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }

    public String getName() {
        return name;
    }

    public String getSoil() {
        return soil;
    }

    public String getOrigin() {
        return origin;
    }

    public VisualParameters getParameters() {
        return parameters;
    }

    public GrowingTips getGrowingTips() {
        return growingTips;
    }

    public String getMultiplying() {
        return multiplying;
    }

    @Override
    public String toString() {
        return "flower{" +
                "name='" + name + '\'' +
                ", soil=" + soil +
                ", origin='" + origin + '\'' +
                ",\n parameters=" + parameters +
                ",\n growingTips=" + growingTips +
                ", multiplying=" + multiplying +
                "}\n";
    }
}
