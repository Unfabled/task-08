package com.epam.rd.java.basic.task8.flowerPackage;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "growingTips")

public class GrowingTips {
    @XmlElementRef(name = "tempreture")
    public Tempreture tempreture;
    @XmlElementRef(name="lighting")
    public Lighting lighting;
    @XmlElementRef(name = "watering")
    public Watering watering;

    public GrowingTips(Tempreture tempreture, Lighting lighting, Watering watering) {
        this.tempreture = tempreture;
        this.lighting = lighting;
        this.watering = watering;
    }

    public GrowingTips() {
    }

    public void setTempreture(Tempreture tempreture) {
        this.tempreture = tempreture;
    }

    public void setLighting(Lighting lighting) {
        this.lighting = lighting;
    }

    public void setWatering(Watering watering) {
        this.watering = watering;
    }

    public Tempreture getTempreture() {
        return tempreture;
    }

    @Override
    public String toString() {
        return "growingTips{" +
                "tempreture=" + tempreture +
                ", lighting=" + lighting +
                ", watering=" + watering +
                '}';
    }
}
