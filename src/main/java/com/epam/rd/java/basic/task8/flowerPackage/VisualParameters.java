package com.epam.rd.java.basic.task8.flowerPackage;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "visualParameters")
public class VisualParameters {
    @XmlElement(name="stemColour")
    private String stemColour;
    @XmlElement(name="leafColour")
    private String leafColour;
    @XmlElementRef (name = "aveLenFlower")
    private AveLenFlower aveLenFlower;

    public VisualParameters() {
    }

    public VisualParameters(String stemColour, String leafColour, AveLenFlower aveLenFlower) {
        this.stemColour = stemColour;
        this.leafColour = leafColour;
        this.aveLenFlower = aveLenFlower;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public void setAveLenFlower(AveLenFlower aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }

    public AveLenFlower getAveLenFlower() {
        return aveLenFlower;
    }

    @Override
    public String toString() {
        return "visualParameters{" +
                "stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlower=" + aveLenFlower +
                '}';
    }
}
