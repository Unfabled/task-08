package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.flowerPackage.*;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            return;
        }

        String xmlFileName = args[0];
        System.out.println("Input ==> " + xmlFileName);

        ////////////////////////////////////////////////////////
        // DOM
        ////////////////////////////////////////////////////////

        // get container
        DOMController domController = new DOMController(xmlFileName);
        Flowers flowersDOM = domController.getFlowersFromXML();

        // sort (case 1)
        flowersDOM.getFlowersList().sort(Comparator.comparing(Flower::getName));

        // save
        String outputXmlFile = "output.dom.xml";
        XMLCreateController controller = new XMLCreateController(outputXmlFile);
        controller.createXMLFromJavaObjects(flowersDOM);

        ////////////////////////////////////////////////////////
        // SAX
        ////////////////////////////////////////////////////////

        // get
        SAXController saxController = new SAXController(xmlFileName);
        Flowers flowersSAX = saxController.getFlowersFromXML();

        // sort  (case 2)
        flowersSAX.getFlowersList().sort((o1, o2) -> {
            AveLenFlower a1 = o1.getParameters().getAveLenFlower();
            AveLenFlower a2 = o2.getParameters().getAveLenFlower();
            return a2.getValue() - a1.getValue();
        });

        // save
        outputXmlFile = "output.sax.xml";
        controller = new XMLCreateController(outputXmlFile);
        controller.createXMLFromJavaObjects(flowersSAX);

        ////////////////////////////////////////////////////////
        // StAX
        ////////////////////////////////////////////////////////

        // get
        STAXController staxController = new STAXController(xmlFileName);
        Flowers flowersSTAX = staxController.getFlowersFromXML();

        // sort  (case 3)
        flowersSTAX.getFlowersList().sort((o1, o2) -> {
            Tempreture a1 = o1.getGrowingTips().getTempreture();
            Tempreture a2 = o2.getGrowingTips().getTempreture();
            return a1.getValue() - a2.getValue();
        });

        // save
        outputXmlFile = "output.stax.xml";
        controller = new XMLCreateController(outputXmlFile);
        controller.createXMLFromJavaObjects(flowersSTAX);
    }

}
