package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flowers;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;

public class XMLCreateController {
        private static String fileName;

        public XMLCreateController(String filePath) {
            fileName = filePath;
        }

    public void createXMLFromJavaObjects(Flowers object) throws JAXBException {
        File file = new File(fileName);
        JAXBContext jaxbContext = JAXBContext.newInstance(Flowers.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(object, file);

    }
}
