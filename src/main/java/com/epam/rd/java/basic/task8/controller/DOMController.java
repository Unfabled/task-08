package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.flowerPackage.*;
import com.epam.rd.java.basic.task8.Flowers;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

    private String xmlFileName;

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public Flowers getFlowersFromXML() throws ParserConfigurationException, IOException, SAXException {
        List<Flower> flowersList = new ArrayList<>();

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new File(xmlFileName));
        NodeList flowers = document.getElementsByTagName("flower");

        for (int i = 0; i < flowers.getLength(); i++) {
            Flower fl = new Flower();
            Node flower = flowers.item(i);
            if (flower.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) flower;
                //setting name
                fl.setName(element.getElementsByTagName("name")
                        .item(0).getTextContent());
                //setting soil
                String soilFl = element.getElementsByTagName("soil")
                        .item(0).getTextContent();
                fl.setSoil(soilFl);
                //setting origin
                fl.setOrigin(element.getElementsByTagName("origin")
                        .item(0).getTextContent());
                //getting visualParameters from flower
                NodeList visualParam = element.getElementsByTagName("visualParameters");
                for (int j = 0; j < visualParam.getLength(); j++) {
                    Element inElement = (Element) visualParam.item(j);
                    if (inElement.getNodeType() == Node.ELEMENT_NODE) {
                        String str = inElement.getElementsByTagName("aveLenFlower").item(0)
                                .getAttributes().item(0).getTextContent();
                        int value = Integer.parseInt(inElement.getElementsByTagName("aveLenFlower")
                                .item(0).getTextContent());
                        VisualParameters visualParameter = new VisualParameters(
                                inElement.getElementsByTagName("stemColour").item(0).getTextContent(),
                                inElement.getElementsByTagName("leafColour").item(0).getTextContent(),
                                new AveLenFlower(str, value)
                        );
                        fl.setParameters(visualParameter);
                    }
                }
                //getting growingTips from flower
                NodeList growingTips = element.getElementsByTagName("growingTips");
                for (int j = 0; j < growingTips.getLength(); j++) {
                    Element inElement = (Element) growingTips.item(j);
                    if (inElement.getNodeType() == Node.ELEMENT_NODE) {
                        String tempStr = inElement.getElementsByTagName("tempreture").item(0)
                                .getAttributes().item(0).getTextContent();
                        int tempValue = Integer.parseInt(inElement.getElementsByTagName("tempreture")
                                .item(0).getTextContent());
                        Tempreture tempreture = new Tempreture(tempStr, tempValue);

                        String waterStr = inElement.getElementsByTagName("watering").item(0)
                                .getAttributes().item(0).getTextContent();
                        int waterValue = Integer.parseInt(inElement.getElementsByTagName("watering")
                                .item(0).getTextContent());
                        Watering watering = new Watering(waterStr, waterValue);

                        String light = inElement.getElementsByTagName("lighting").item(0)
                                .getAttributes().item(0).getTextContent();
                        Lighting lighting = new Lighting(light);

                        fl.setGrowingTips(new GrowingTips(tempreture, lighting, watering));
                    }
                }
                //setting multiplying
                String mult = element.getElementsByTagName("multiplying")
                        .item(0).getTextContent();
                fl.setMultiplying(mult);
            }
            flowersList.add(fl);
        }

        return new Flowers(flowersList);
    }
}
