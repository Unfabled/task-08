package com.epam.rd.java.basic.task8;

import java.util.List;

import com.epam.rd.java.basic.task8.flowerPackage.Flower;

import javax.xml.bind.annotation.*;

@XmlRootElement (name = "flowers")
@XmlAccessorType(XmlAccessType.FIELD)
public class Flowers {
    @XmlAttribute(name = "xmlns")
    private static final String XMLNS = "http://www.nure.ua";
    @XmlAttribute(name = "xmlns:xsi")
    private static final String XSI = "http://www.w3.org/2001/XMLSchema-instance";
    @XmlAttribute(name = "xsi:schemaLocation")
    private static final String SCHEMALOCATION = "http://www.nure.ua input.xsd ";
    @XmlElementRef(name = "flower")
    private List<Flower> flowersList;

    public Flowers() {
    }

    public Flowers(List<Flower> flowersList) {
        this.flowersList = flowersList;
    }

    public void setFlowersList(List<Flower> flowersList) {
        this.flowersList = flowersList;
    }

    public List<Flower> getFlowersList() {
        return flowersList;
    }

    @Override
    public String toString() {
        return "flowers{" +
                "flowersList=" + flowersList +
                '}';
    }
}
